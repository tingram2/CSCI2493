public class Recurse {
	public void recurse(int count){
		System.out.println(count + " recursion");
		if (count <= 0){
			System.out.println("Done");
		}else{
			recurse(count - 1);
		}
	}

	public static void main(String[] args){
		Recurse m = new Recurse();
		m.recurse(10);
	}
}
