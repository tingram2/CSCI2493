import java.util.*;
public class Pez {
        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                Stack<String> stack = new Stack<String>();

                System.out.println("Here is our empty pez dispenser: " + stack);
                System.out.println("Let's fill it with 10 colored candies."); 
		
	        for(int i = 0; i < 10; i++){

        	        System.out.println("Enter a color: ");
        	        String value = sc.nextLine();
			stack.push(value);
		}


		System.out.println("Here is our loaded pez dispenser: " + stack);
		System.out.println("Let's empty it one by one.");
	
		for (int i = 0; i < 10; i++){
			stack.pop();
			System.out.println(stack);
		}

		System.out.println("Now our pez dispenser is empty: " + stack);

    }

}
