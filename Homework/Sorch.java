import java.util.Arrays; 
  
public class Sorch { 
    public static void main(String[] args) 
    { 
        int[] arr = { 13, 7, 6, 45, 21, 9, 101, 102 }; 
	
	System.out.printf("Original array: %s", 
			Arrays.toString(arr));
	System.out.println();
  
	Arrays.sort(arr); 
  
	System.out.printf("Sorted array: %s", 
			Arrays.toString(arr)); 
	System.out.println();
    
	int intKey = 45; 

	System.out.println(intKey + " found at index = "
                           +Arrays.binarySearch(arr,intKey)); 

	} 
}
