import java.util.*;

public class LoadADS {
        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                Stack<String> stack = new Stack<String>();

                System.out.println("Here is our empty stack: " + stack);
                System.out.println("Let's load it."); 
		
	        for(int i = 0; i < 3; i++){

        	        System.out.println("Enter three names: ");
        	        String value = sc.nextLine();
			stack.push(value);
		}


		System.out.println("Here is our finished stack: " + stack);
	}
}
